var cmSurvey = angular.module("cmSurvey", ["ngTap","slickCarousel","ui.router","ngCookies","ngStorage","ngAnimate",'signature']);
var loginAuth = false;
var surveyDetails = {
  signUpInfo : [],
  holidayMeter : [],
  lifeStyle : []
};
var holidayMeterScore = 0;
var lifeStyleParagraph = "";
var paddition = 0;
var signData = "";


cmSurvey.config(["$stateProvider", "$urlRouterProvider", function($stateProvider, $urlRouterProvider) {

    $urlRouterProvider.otherwise("login");

    $stateProvider.state("login", {
      stateName : "login",
      name: 'login',
      url: "/login",
      templateUrl: "partials/login/content.html",
      controller: 'LoginController',
      bodyClass : "login"
    });

    $stateProvider.state("welcome", {
      stateName : "welcome",
      name: 'welcome',
      url: "/welcome",
      templateUrl: "partials/welcome/content.html",
      controller: "welcomeCtrl",
      bodyClass : "bgw"
    });

    $stateProvider.state("signup", {
      stateName : "signup",
      name: 'signup',
      templateUrl: "partials/userinfo/content.html" ,
      controller: "questionearCtrl",
      bodyClass : "bg1"
    });

    $stateProvider.state("lifestyle", {
      stateName : "lifestyle",
      name: 'lifestyle',
      templateUrl: "partials/travelometer/content.html" ,
      controller: "questionearCtrl",
      bodyClass : "bg9"
    });

    $stateProvider.state("meter", {
      stateName : "meter",
      name: 'meter',
      templateUrl: "partials/stresstest/content.html" ,
      controller: "questionearCtrl",
      bodyClass : "bg24"
    });

    $stateProvider.state("scorecard", {
      stateName : "scorecard",
      name: 'scorecard',
      templateUrl: "partials/results/content.html" ,
      controller: "scorecardCtrl",
      bodyClass : "bg17"
    });

    $stateProvider.state("thanks", {
      stateName : "thanks",
      name: 'thanks',
      templateUrl: "partials/thanks/content.html" ,
      controller: "thanksCtrl",
      bodyClass : "bg33 thanks"
    });

}]);



cmSurvey.controller('mainController',['$rootScope','$scope','$state','$timeout', function($rootScope,$scope,$state,$timeout){

  var stateName = "";

  $rootScope.$on('$stateChangeSuccess', function(event, toState, toParams, fromState, fromParams) {

    stateName = toState.stateName;
    $(".background").removeClassRegex("bg");
    var bodyClass = toState.bodyClass;


    if(stateName){
      checkState(stateName);
    }

    function checkState(stateName){
      switch(stateName){
          case "welcome"    :     makeMeInVisible();
                                  break;

          case "signup"     :     paddition = 0;
                                  bindSignaturePad();
                                  makeMeVisible();
                                  break;

          case "lifestyle"  :     paddition = 5;
                                  var a = $rootScope.userInfoDataArray.age;
                                  var b = $rootScope.userInfoDataArray.children;
                                  if(a < 30 && b == 0){
                                    bodyClass = "bg9";
                                  } else if(a < 30 && b > 0){
                                    bodyClass = "bg10";
                                  } else if(a > 29 && a < 56 && b == 0){
                                    bodyClass = "bg11";
                                  }else if(a > 29 && a < 56 && b > 0){
                                    bodyClass = "bg12";
                                  } else if(a > 55 && b == 0){
                                    bodyClass = "bg13";
                                  } else if(a > 55 && b > 0){
                                    bodyClass = "bg14";
                                  }
                                  //postUserData();
                                  makeMeVisible();
                                  break;

          case "meter"      :     paddition = 10;
                                  makeMeVisible();
                                  moreVideos();
                                  break;

          case "scorecard"  :     moreVideos();
                                  makeMeInVisible();
                                  break;

          case "thanks"     :     moreVideos();
                                  makeMeInVisible();                                  
                                  break;
      }
    }



    function  makeMeVisible() {
      $timeout( function(){
         angular.element(".navigation").addClass("active");
      }, 1000 );
      angular.element(".cm_progressContainer").addClass("active");
    };

    function  makeMeInVisible() {
        angular.element(".navigation").removeClass("active");
        angular.element(".cm_progressContainer").removeClass("active");
    };

    function moreVideos() {
      $(document).ready(function () {
        var btn = document.querySelector(".wmv");
        if(btn){
          var srcCOntentId = "#hidden-content-"+$rootScope.videos_list_id;
          btn.addEventListener("click",function () {
              $.fancybox.open({
                src  : srcCOntentId,
                type : 'inline',
                opts : {
                  afterShow : function( instance, current ) {
                  }
                }
              });
          });
        }
      });
    }

    function signPadReset() {
      $(document).ready(function () {
        var canvas = angular.element(".signature").find("canvas")[0];
        var _signaturePad = new SignaturePad(canvas);
        _signaturePad.clear($rootScope.needToBeSigned=true);
      });
    }

    $rootScope.getBgClass = function (cls) {
      if(cls)         return cls;
      if(bodyClass)   return bodyClass;
    };

  });

  $scope.prevScreen = function () {
      if(stateName == "meter"){
        navigateTo = "lifestyle"
      } else if(stateName == "lifestyle"){
        navigateTo = "signup"
      }
      $state.go(navigateTo);
  };

  $rootScope.pageNum = 1;

  $rootScope.updateUserInfo = function (name,age,ms,noc,sign) {

  };
  $rootScope.updateHolidayMeter = function (A,B,C,D,E) {
      var _total_points = calculateScore(A) + calculateScore(B) + calculateScore(C) + calculateScore(D);
      var _percentage = (_total_points/80)*100;
      holidayMeterScore = Math.round(_percentage);
  };

  $rootScope.getMeterPercentage = function (A,B,C,D) {
      var _total_points = calculateScore(A) + calculateScore(B) + calculateScore(C) + calculateScore(D);
      var _percentage = (_total_points/80)*100;
      return Math.round(_percentage);
  };

  $rootScope.updateLifeStyle = function (A,B,C,D,E) {

  };

  $rootScope.getParagraph = function (A,B,C,D,vids_cat) {
      var resPer = $rootScope.getMeterPercentage(A,B,C,D);
      var _result_para = "";
      
      if(resPer > 9 && resPer < 31){
        _result_para = "We’re worried that you’re under tremendous stress. You need to go on a fun family holiday right away!";
      } else if(resPer > 30 && resPer < 61){
        _result_para = "You seem to be a little stressed. You should go on fun family holidays regularly.";
      } else if(resPer > 60 && resPer < 81){
        _result_para = "Great! This shows you’re a happy person. To maintain this, you must go on fun family holidays regularly.";
      } else if(resPer > 80){
        _result_para = "Wow! This shows you love travelling. Keep up the spirit and take regular family holidays to explore newer destinations.";
      }

      lifeStyleParagraph = _result_para;
      $rootScope.lifeStyleParagraph = lifeStyleParagraph;
      $rootScope.videos_list_id = vids_cat;
  };

  $rootScope.checkHmValidation = function () {
      $rootScope.hShhowMsgs = false;
  };

  $rootScope.checkLifeValidation = function () {
      $rootScope.lShhowMsgs = false;
       
  };

   function bindSignaturePad() {
    $(document).ready(function () {
      var canvas = angular.element(".signature").find("canvas")[0];
      var _signaturePad = new SignaturePad(canvas, {
          onEnd : function () {
            $rootScope.needToBeSigned = false;
            signData = _signaturePad.toDataURL();
          }
      });
    });
  };

  $rootScope.userInfoDataArray = {};
  $rootScope.lifestyleData = {};
  $rootScope.metereData = {};

  calculateScore = function (choice) {
    var ch = parseInt(choice);
    var _value = 0;
      switch(ch){
        case 0 : _value = 20;
                 break;
        case 1 : _value = 15;
                 break;
        case 2 : _value = 12;
                 break;
        case 3 : _value = 7;
                 break;
        case 4 : _value = 3;
                 break;
      }
      return _value;
  };



}]);

cmSurvey.controller("signupCtrl", ["$rootScope","$scope",function($rootScope,$scope) {
  $rootScope.needToBeSigned = true;
  $scope.signUpInfo = function (e) {
      $rootScope.userInfoDataArray.sign = signData;
  };
  $scope.toggleSign = function () {
    $rootScope.needToBeSigned = true;
  }
}]);



cmSurvey.controller("meterCtrl", ["$rootScope","$scope",function($rootScope,$scope) {
  $rootScope.needToBeSigned = true;
  $scope.signUpInfo = function (e) {
  };
  $scope.toggleSign = function () {
    $rootScope.needToBeSigned = true;
  }
}]);


cmSurvey.controller("loginCtrl", ["$scope",function($scope) {

}]);



cmSurvey.controller("welcomeCtrl", ["$scope","$localStorage",function($scope,$localStorage) {

  // var choiceList = [
  //   {"questionId":"1","choiceId":"4"},
  //   {"questionId":"2","choiceId":"5"},
  //   {"questionId":"3","choiceId":"11"},
  //   {"questionId":"4","choiceId":"17"},
  //   {"questionId":"5","choiceId":"22"},
  //   {"questionId":"6","choiceId":"28"},
  //   {"questionId":"7","choiceId":"33"},
  //   {"questionId":"8","choiceId":"37"},
  //   {"questionId":"9","choiceId":"41"}
  // ];

  // var choiceList = [
  //   {questionId:1,choiceId:4},
  //   {questionId:2,choiceId:5},
  //   {questionId:3,choiceId:11},
  //   {questionId:4,choiceId:17},
  //   {questionId:5,choiceId:22},
  //   {questionId:6,choiceId:28},
  //   {questionId:7,choiceId:33},
  //   {questionId:8,choiceId:37},
  //   {questionId:9,choiceId:4}
  // ];

  // let temp=$localStorage.currentUser;

  // var cstr = JSON.stringify(choiceList);
  // var clstring = cstr.replace(/\"([^(\")"]+)\":/g,"$1:");
            
  // let surveyData = {
  //   SurveyId:'r1',
  //   customerdetailsId: 55,
  //   quesAndAnsList:choiceList,
  //   quesAndAnsListlength:'1'
  // };

  // // let urlParameters = Object.entries(sendData).map(e => encodeURIComponent(e[0])+"="+encodeURIComponent(e[1])).join('&');

  // let urlParameters = "SurveyId=r1&customerdetailsId=27";

  // console.log(urlParameters);

  // // let urlParameters = 'SurveyId=r1&customerdetailsId=25&quesAndAnsList=0&quesAndAnsListlength=1';
  
  // fetch('http://159.65.159.58:8080/api/SurveyConducted/saveSurveyConducted', {
  //   method: 'POST',
  //   headers: {
  //       'Accept': 'application/json',
  //       'Content-Type': 'application/x-www-form-urlencoded',
  //       'x-auth-token': temp.token,
  //   },
  //   body: 'urlParameters'
  //   }).then((response) => {
  //     console.log(response);
  //         return response.json();
  //   }).then((responseJson) => {
  //       console.log(responseJson);
  //   }).catch((error) => {
  //     console.error(error);
  //   });


}]);

cmSurvey.config(['slickCarouselConfig', function (slickCarouselConfig) {
    slickCarouselConfig.dots = false;
    slickCarouselConfig.autoplay = false;
  }]).controller('questionearCtrl',["$rootScope","$scope","$timeout","$state", function ($rootScope,$scope, $timeout,$state) {
    $scope.navActive = true; 
    $scope.slickConfig1Loaded = true;
    $scope.slickCurrentIndex = 0;
    $rootScope.slickConfig = {
      dots: false,
      autoplay: false,
      initialSlide: 0,
      infinite: false,
      slidesToScroll: 1,
      variableWidth: true,
      arrows:false,
      method: {},
      event: {
        afterChange: function (event, slick, currentSlide, nextSlide) {
          $scope.slickCurrentIndex = currentSlide;
          $rootScope.pageNum = paddition+currentSlide+1;
          prevScreenFn();
          $(slick.$slides[currentSlide]).find("input").focus();
          var prevCard = slick.$slides[currentSlide].previousElementSibling;
          var field;
          if(prevCard) field = prevCard.lastChild.lastChild.attributes["data-field"].nodeValue;
          var fieldValue = $(slick.$slides[currentSlide].previousElementSibling).find("input").val();
          $rootScope.changeBgFn(field,fieldValue);
          progressBarUpdate();
        },
        breakpoint: function (event, slick, breakpoint) {

        },
        init: function (event, slick,currentSlide) {
          $rootScope.pageNum = paddition+1;
          $(slick.$slides[currentSlide]).find("input").focus();
          prevScreenFn();
          document.addEventListener('keyup', function (e) {
            if(e.keyCode == 13 || e.keyCode == 9){
              $scope.slickConfig.method.slickNext();
            }
          });

          var lbls = document.querySelectorAll(".card .inputs label");
          for (var i = 0;i < lbls.length; i++) {
            lbls[i].addEventListener('click', function() {
              $scope.slickConfig.method.slickNext();
            });
          }
        },
        setPosition: function (evnet, slick) {

        }
        
      }
    };

    function progressBarUpdate() {
      var scrollPercentage = 0;
      var totalInputs = Object.keys($rootScope.userInfoDataArray).length + Object.keys($rootScope.lifestyleData).length + Object.keys($rootScope.metereData).length;
      if(!$rootScope.userInfoDataArray.phone && !$rootScope.userInfoDataArray.email){
        scrollPercentage = (totalInputs/14) * 100;
      } else if(!$rootScope.userInfoDataArray.phone || !$rootScope.userInfoDataArray.email){
        scrollPercentage = (totalInputs/15) * 100;
      } else {
        scrollPercentage = (totalInputs/16) * 100;
      }

      if(scrollPercentage < 5){
        $(".cm_progressBar").addClass("start");
      } else if(scrollPercentage > 99){
        $(".cm_progressBar").addClass("done");
      } else{
        $(".cm_progressBar").removeClass("done");
        $(".cm_progressBar").removeClass("start");
      }
      
      $(".cm_progressBar").css("width",scrollPercentage+"%");  

    }

    function prevScreenFn() {
      if($rootScope.pageNum == 6 || $rootScope.pageNum == 11){
        $(".back.prev-back").show();
        $(".back.slick-back").hide();
      } else{
        $(".back.prev-back").hide();
        $(".back.slick-back").show();
      }
    }
     

    $rootScope.changeBgFn = function (field,value) {
      //console.log(field+" "+value);
      var uage = parseInt($rootScope.userInfoDataArray.age);
      var ukids = parseInt($rootScope.userInfoDataArray.children);
      var vdest = parseInt($rootScope.metereData.C);

      var bgCls = "welcome";
      switch(field){
        case "name" : bgCls = "bg2";
                      break;
        case "age" : 
                      if(value){
                        var a = parseInt(value);
                        if(a < 31){
                          bgCls = "bg3";
                        } else if(a > 30 && a < 56){
                          bgCls = "bg4";
                        }else if(a > 55){
                          bgCls = "bg5";
                        }
                      }
                      break;
        case "mstatus" : bgCls = "bg6";
                          break;

        case "kids" : bgCls = "bg7";
                          break;

        case "pemail" : bgCls = "bg8";
                          break;

        case "signature" : bgCls = "bg8";
                          break;


        case "hwf" :    if(uage < 30 && ukids == 0){
                          bgCls = "bg15";
                        }else if(uage < 30 && ukids > 0){
                          bgCls = "bg16";
                        } else if(uage > 29 && uage < 56 && ukids == 0){
                          bgCls = "bg17";
                        }else if(uage > 29 && uage < 56 && ukids > 0){
                          bgCls = "bg18";
                        } else if(uage > 55 && ukids == 0){
                          bgCls = "bg19";
                        } else if(uage > 55 && ukids > 0){
                          bgCls = "bg20";
                        }
                        break;

        case "fuh" :    bgCls = "bg21";
                        break;

        case "newdest" : bgCls = "bg22";
                          break;

        case "intdest" : bgCls = "bg23";
                         break;

        case "advcat" : bgCls = "bg24";
                         break;

        case "wiw" : if(uage < 30 && vdest == 0){
                          bgCls = "bg25";
                        } else if(uage < 30 && vdest == 1){
                          bgCls = "bg26";
                        } else if(uage < 30 && vdest == 2){
                          bgCls = "bg27";
                        } else if(uage < 30 && vdest == 3){
                          bgCls = "bg28";
                        }

                        if(uage > 30 && vdest == 0){
                          bgCls = "bg29";
                        } else if(uage > 30 && vdest == 1){
                          bgCls = "bg30";
                        } else if(uage > 30 && vdest == 2){
                          bgCls = "bg31";
                        } else if(uage > 30 && vdest == 3){
                          bgCls = "bg32";
                        }
                        break;

        case "lfw" :    if(uage < 31){
                          bgCls = "bg33";
                        } else if(uage > 30 && uage < 56){
                          bgCls = "bg34";
                        }else if(uage > 55){
                          bgCls = "bg35";
                        }
                        break;

        case "hiy" :    bgCls = "bg36";
                        break;

        case "dvac" :   bgCls = "bg36";
                        break;


      }
      changeBgClassText(bgCls);
      
    }
    
    var changeBgClassText = function (cls) {
      //console.log(cls);
      var bg = $(".background");
      bg.removeClassRegex("bg");
      bg.addClass(cls);
    };


    $scope.resetForm = function () {
      $scope.this.userLifestyleForm.vacationinyear.$setValidity('someValidator', null);
    };

    $scope.validateFrom = function () {
      $scope.this.userLifestyleForm.vacationinyear.$setValidity('someValidator', true);
    };
    

    $scope.makeMeActive = function (item) {
      var i = item.currentTarget.parentElement.parentElement.dataset.slickIndex;
      if(!(i==$scope.slickCurrentIndex))
        $scope.slickConfig.method.slickGoTo(i);
    };

}]);

cmSurvey.controller("lifestyleCtrl", ["$scope",function($scope) {
}]);

cmSurvey.controller("meterCtrl", ["$scope",function($scope) {
}]);


cmSurvey.controller("scorecardCtrl", ["$rootScope","$scope",function($rootScope,$scope) {
  $scope.$on('$viewContentLoaded', function(e){ 
      $rootScope.getParagraph($rootScope.metereData.A,$rootScope.metereData.B,$rootScope.metereData.D,$rootScope.metereData.E,$rootScope.lifestyleData.D);
      var percentage = $rootScope.getMeterPercentage($rootScope.metereData.A,$rootScope.metereData.B,$rootScope.metereData.D,$rootScope.metereData.E);
      $scope.percentage = percentage+"%";
      var r = 100;
      var circles = document.querySelectorAll('.circle');
      var total_circles = circles.length;
      for (var i = 0; i < total_circles; i++) {
          circles[i].setAttribute('r', r);
      }
      var meter_dimension = (r * 2) + 200;
      var wrapper = document.querySelector('#wrapper');
      if(percentage < 41){
        $(wrapper).addClass("low");
      }
      wrapper.style.width = meter_dimension + 'px';
      wrapper.style.height = meter_dimension + 'px';
      var cf = 2 * Math.PI * r;
      var semi_cf = cf / 2;
      var semi_cf_1by3 = semi_cf / 3;
      var semi_cf_2by3 = semi_cf_1by3 * 2;
      document.querySelector('#outline_curves').setAttribute('stroke-dasharray', semi_cf + ',' + cf);
      document.querySelector('#low').setAttribute('stroke-dasharray', semi_cf + ',' + cf);
      document.querySelector('#avg').setAttribute('stroke-dasharray', semi_cf_2by3 + ',' + cf);
      document.querySelector('#high').setAttribute('stroke-dasharray', semi_cf_1by3 + ',' + cf);
      document.querySelector('#outline_ends').setAttribute('stroke-dasharray', 0 + ',' + (semi_cf - 2));
      document.querySelector('#mask').setAttribute('stroke-dasharray', semi_cf + ',' + cf);
      var mask = document.querySelector('#mask');
      var meter_needle =  document.querySelector('#meter_needle');
      function range_change_event(p) {
          var percent = p;
          var meter_value = semi_cf - ((percent * semi_cf) / 100);
          mask.setAttribute('stroke-dasharray', meter_value + ',' + cf);
          meter_needle.style.transform = 'rotate(' + (270 + ((percent * 180) / 100)) + 'deg)';
      }
      setTimeout(range_change_event(percentage),5000);
  });

}]);

cmSurvey.run(['$rootScope', '$http', '$location', '$localStorage','$state', function($rootScope, $http, $location, $localStorage,$state) {
    if ($localStorage.currentUser) {
        $http.defaults.headers.common.Authorization = 'Bearer ' + $localStorage.currentUser.token;
        loginAuth = true;
    }
    $rootScope.$on('$locationChangeStart', function (event, next, current) {
        var publicPages = ['/login'];
        var restrictedPage = publicPages.indexOf($location.path()) === -1;
        if (restrictedPage && $localStorage.currentUser === undefined) {
            $location.path('login');
        }
    });
}]);

cmSurvey.controller('LoginController',['$scope', '$rootScope', '$location', 'AuthenticationService','$state',
  function ($scope,  $rootScope, $location, AuthenticationService,$state) {
      AuthenticationService.Logout();
      $scope.dataLoading = true;
      $scope.login = function () {
      
            var mycmSurveyElement = document.getElementById("mycmSurvey");
        
            if (!document.mozFullScreen && !document.webkitFullScreen) {
              if (mycmSurveyElement.mozRequestFullScreen) {
                mycmSurveyElement.mozRequestFullScreen();
              } else {
                mycmSurveyElement.webkitRequestFullScreen(Element.ALLOW_KEYBOARD_INPUT);
              }
            } else {
              if (document.mozCancelFullScreen) {
                document.mozCancelFullScreen();
              } else {
                document.webkitCancelFullScreen();
              }
            }
      
          $scope.dataLoading = true;
          AuthenticationService.Login($scope.username, $scope.password, function (result) {
              if (result === true) {
                  $state.go('welcome');
                  loginAuth = true;
              } else {
                  $scope.dataLoading = false;
              }
          });
      };
  }
]);

cmSurvey.factory('AuthenticationService',['$http', '$localStorage',function($http, $localStorage) {
        var service = {};
        service.Login = Login;
        service.Logout = Logout;
        return service;
        function Login(username, password, callback) {
                fetch('http://159.65.159.58:8080/api/api/login',
                {
                  method: 'POST',
                  headers: new Headers({
                    'Accept': 'application/json',
                    'content-type' : 'application/x-www-form-urlencoded' ,
                  }),
                  body:"username="+username+"&password="+password
                }).then((response) => {
                    if (response.status===200) {
                      return response.json();
                    }
                    return response;
                }).then((responseJson) => {
                    if (responseJson.access_token) {
                        $localStorage.currentUser = { username: username, token: responseJson.access_token };
                        $http.defaults.headers.common.Authorization = 'Bearer ' + responseJson.access_token;
                        callback(true);
                    } else {
                        callback(false);
                    }
                }).catch((error) => {
                  console.error(error);
                });
        }
        function Logout() {
            delete $localStorage.currentUser;
            $http.defaults.headers.common.Authorization = '';
        }
    }
]);
 


cmSurvey.controller("thanksCtrl", ["$scope","$rootScope","$location","$window","$localStorage",function($scope,$rootScope,$location, $window,$localStorage) {
  $scope.reSurvey = function () {
    saveCurrentUserSurvey();
  }

    function saveCurrentUserSurvey() {
      postSignupInfo();
    };
    
    function postSignupInfo() {

      var a = $rootScope.userInfoDataArray;

      var _ud = {};

      _ud.name = a.name;
      _ud.age = a.age;
      _ud.carOwned = '0';
      if(a.mstatus == "0") _ud.maritalStatus = "Engaged";
      else if(a.mstatus == "1") _ud.maritalStatus = "Married";
      else _ud.maritalStatus = "Single";
      _ud.noOfChildren = a.children;
      _ud.phoneNumber = a.phone;
      _ud.emailid = a.email;
      _ud.image = a.sign;

      let sendData = _ud;
        
      let temp=$localStorage.currentUser;
 
      let urlParameters = Object.entries(sendData).map(e => encodeURIComponent(e[0])+"="+encodeURIComponent(e[1])).join('&');

      fetch('http://159.65.159.58:8080/api/CustomerDetails/createCustomerDetails', {
        method: 'POST',
        headers: {
          'Accept': 'application/json',
          'Content-Type': 'application/x-www-form-urlencoded',
          'x-auth-token': temp.token,
        },
        body: urlParameters
        }).then((response) => {
          if(response.status===200 || response.status===400)
              return response.json();
            return response;
        }).then((responseJson) => {
            //console.log(responseJson);
            $localStorage.NewCustomer = { customerDetailsId : responseJson.customerDetailsId };
            postSurveyInfo(responseJson.customerDetailsId,temp.token);
        }).catch((error) => {
          //console.error(error);
        });
              
    };

    function postSurveyInfo(customerId,token) {

      var b = $rootScope.metereData;
      var c = $rootScope.lifestyleData;
      
      var choiceList = [];
      var v = 0;
      var ct =0;

      for(var k in b){
        v = v+1;
        var _c = "";
        if(b.hasOwnProperty(k)){
          if(k == "A")
            _c = retriveChoiceAb(b[k]);
          else if(k == "B")
            _c = retriveChoiceBb(b[k]);
          else if(k == "C")
            _c = retriveChoiceCb(b[k]);
          else if(k == "D")
            _c = retriveChoiceDb(b[k]);
          else if(k == "E")
            _c = retriveChoiceEb(b[k]);

          
          var t = {
            questionId:v,
            choiceId : _c
          }

          choiceList.push(t);
        }
      }

      for(var k in c){
        v = v+1;
        var _c = "";
        if(b.hasOwnProperty(k)){
          if(k == "A")
            _c = retriveChoiceAc(b[k]);
          else if(k == "B")
            _c = retriveChoiceBc(b[k]);
          else if(k == "C")
            _c = retriveChoiceCc(b[k]);
          else if(k == "D")
            _c = retriveChoiceDc(b[k]);

          
          var t = {
            questionId:v,
            choiceId : _c
          }

          choiceList.push(t);
        }
      }

      function retriveChoiceAb(choice) {
        switch(choice){
          case "0" : return 1;
          case "1" : return 2;
          case "2" : return 3;
          case "3" : return 4;
          case "4" : return 5;
        }
      } 

      function retriveChoiceBb(choice) {
        switch(choice){
          case "0" : return 6;
          case "1" : return 7;
          case "2" : return 8;
          case "3" : return 9;
          case "4" : return 10;
        }
      } 

      function retriveChoiceCb(choice) {
        switch(choice){
          case "0" : return 11;
          case "1" : return 12;
          case "2" : return 13;
          case "3" : return 14;
        }
      } 

      function retriveChoiceDb(choice) {
        switch(choice){
          case "0" : return 15;
          case "1" : return 16;
          case "2" : return 17;
          case "3" : return 18;
          case "4" : return 19;
        }
      } 


      function retriveChoiceEb(choice) {
        switch(choice){
          case "0" : return 20;
          case "1" : return 21;
          case "2" : return 22;
          case "3" : return 23;
          case "4" : return 24;
        }
      } 

      function retriveChoiceAc(choice) {
        switch(choice){
          case "0" : return 25;
          case "1" : return 26;
          case "2" : return 27;
          case "3" : return 28;
          case "4" : return 29;
        }
      } 

      function retriveChoiceBc(choice) {
        switch(choice){
          case "0" : return 30;
          case "1" : return 31;
          case "2" : return 32;
          case "3" : return 33;
        }
      } 

      function retriveChoiceCc(choice) {
        switch(choice){
          case "0" : return 34;
          case "1" : return 35;
          case "2" : return 36;
          case "3" : return 37;
        }
      } 

      function retriveChoiceDc(choice) {
        switch(choice){
          case "0" : return 38;
          case "1" : return 39;
          case "2" : return 40;
          case "3" : return 41;
        }
      } 

      //console.log(choiceList);  //Array of choice lists objects
      if(customerId){

		  let surveyData = {
	        SurveyId:1,
	        customerdetailsId: customerId,
	        quesAndAnsList: choiceList,
	        quesAndAnsListlength:9
	      };

	      //console.log(surveyData);

	      fetch('http://159.65.159.58:8080/api/SurveyConducted/saveSurveyConducted', {
	          method: 'POST',
	          headers: {
	              'Accept': 'application/json',
	              'Content-Type': 'application/json',
	              'x-auth-token': token,
	          },
	          body: JSON.stringify(surveyData)
	          }).then((response) => {
	              return response.json();
	          }).then((responseJson) => {
	              //console.log(responseJson);
	              if(responseJson.success)
	              	$window.location.reload();
	          }).catch((error) => {
	            //console.error(error);
	          });

	        };
	    }

}]);

cmSurvey.controller("videoCtrl",['$scope','$state','$timeout',function($scope,$state,$timeout) {
  $scope.videopopup = function (id) {
        //console.log("event " + e);
        //var videoId = e.currentTarget.attributes[2].nodeValue;
        var videoId = id;
        var url = "https://www.youtube.com/watch?v="+videoId+"&autoplay=1&rel=0&controls=0&showinfo=0&disablekb=1";
        $.fancybox.open({
          src : url,
          opts : {
            iframe: {
                tpl:'<iframe id="fancybox-frame-45720" name="fancybox-frame-45720" class="fancybox-iframe" frameborder="0" vspace="0" hspace="0" webkitAllowFullScreen mozallowfullscreen allowFullScreen allowtransparency="true" src=""></iframe>',
            },
            btnTpl: {
                close:
                    '<a href="javascript:;" class="btn big exit" id="skipit">Skip</a>',
            },
            touch: false,
            beforeShow : function( instance, current ) {
              var id = "fancybox-frame-45720";
              $(document).ready(function () {
                  var player = new YT.Player(id, {
                        events: {
                            'onReady': onPlayerReady,
                            'onStateChange': onPlayerStateChange
                        }
                  });
                  $("#skipit").on("click",function () {
                    $.fancybox.close();
                    $.fancybox.destroy();
                    $timeout(function () {
                      $state.go("thanks");
                    }, 1000); 
                  });
              });
            }
          }
        });
  }

  function onPlayerReady(event) {
      event.target.playVideo();
  }

  function onPlayerStateChange(event) {
      if (event.data === 0) {
        $.fancybox.destroy();
        $state.go("thanks");
      }
  }
}]);

$.fn.removeClassRegex = function (filter) {
    $(this).removeClass(function (index, className) {
        return (className.match(new RegExp("\\S*" + filter + "\\S*", 'g')) || []).join(' ')
    });
    return this;
};