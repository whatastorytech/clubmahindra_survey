var cmSurvey = angular.module("cmSurvey", ["ngTap","slickCarousel","ui.router","ngCookies","ngStorage","ngAnimate",'signature']);
var loginAuth = false;
var surveyDetails = {
  signUpInfo : [],
  holidayMeter : [],
  lifeStyle : []
};
var holidayMeterScore = 0;
var lifeStyleParagraph = "";
var paddition = 0;
var signData = "";


cmSurvey.config(["$stateProvider", "$urlRouterProvider", function($stateProvider, $urlRouterProvider) {

    $urlRouterProvider.otherwise("login");

    $stateProvider.state("login", {
      stateName : "login",
      name: 'login',
      url: "/login",
      templateUrl: "partials/login/content.html",
      controller: 'LoginController',
      bodyClass : "login"
    });

    $stateProvider.state("welcome", {
      stateName : "welcome",
      name: 'welcome',
      url: "/welcome",
      templateUrl: "partials/welcome/content.html",
      controller: "welcomeCtrl",
      bodyClass : "welcome"
    });

    $stateProvider.state("signup", {
      stateName : "signup",
      name: 'signup',
      templateUrl: "partials/signup/content.html" ,
      controller: "questionearCtrl",
      bodyClass : "signup"
    });

    $stateProvider.state("lifestyle", {
      stateName : "lifestyle",
      name: 'lifestyle',
      templateUrl: "partials/lifestyle/content.html" ,
      controller: "questionearCtrl",
      bodyClass : "signup"
    });

    $stateProvider.state("meter", {
      stateName : "meter",
      name: 'meter',
      templateUrl: "partials/lifestyle2/content.html" ,
      controller: "questionearCtrl",
      bodyClass : "signup"
    });

    $stateProvider.state("scorecard", {
      stateName : "scorecard",
      name: 'scorecard',
      templateUrl: "partials/results/content.html" ,
      controller: "scorecardCtrl",
      bodyClass : "scorecard"
    });

    $stateProvider.state("thanks", {
      stateName : "thanks",
      name: 'thanks',
      templateUrl: "partials/thanks/content.html" ,
      controller: "thanksCtrl",
      bodyClass : "thanks"
    });

}]);



cmSurvey.controller('mainController',['$rootScope','$scope','$state','$timeout', function($rootScope,$scope,$state,$timeout){

  var stateName = "";

  $rootScope.$on('$stateChangeSuccess', function(event, toState, toParams, fromState, fromParams) {

    stateName = toState.stateName;
    var bodyClass = toState.bodyClass;

    function animateLeaf() {

      var welcomeLeafPath = "M663.917,370.8C677.351,250.757,539.757,65.927,410.397,18.473C354.493-1.895,284.505-11.646,238.785,37.758c-39.87,42.903-18.635,100.324,5.417,148.211c20.368,40.303,5.2,57.638-31.636,72.805c-37.269,15.384-73.456,34.019-107.691,55.254C48.537,348.915,0,400.052,0,461.157c0,11.268,1.733,22.968,5.417,35.103C104.008,823.451,657.849,425.187,663.917,370.8z",
          welcomeLeafColor = "#F8992D",

          surveyLeafPath = "M799.127-179.142c-17.768-40.303-65.005-211.916-150.811-186.781c-74.756,22.102-129.576,268.47,79.306,613.646c45.07,74.322,103.574,69.555,150.161-3.684c88.84-139.11,234.884-564.026,147.778-694.469c-7.151-10.617-15.601-15.384-24.702-15.384C936.937-466.03,831.629-234.829,799.127-179.142z",
          surveyLeafColor = "#00B5E2",

          scorecardLeafPath = "M928.92,292.144c-58.721,38.353-73.672,89.707-47.887,155.795c16.901,43.337,180.28,404.764,401.947,237.051c123.943-93.607-19.501-195.665-29.469-202.165c-34.236-22.102-34.019-35.536,4.55-56.554c73.456-39.87,306.39-204.115,143.878-291.655C1293.814,76.544,998.475,246.857,928.92,292.144z",
          scorecardLeafColor = "#FFD14F",

          thanksLeafPath = "M622.53,854.653c-59.154,70.639-214.733,298.806-284.071,192.198c-174.43-267.82,410.614-665.867,456.118-595.878c174.646,252.869,73.672,736.505-63.705,806.494c-15.601,8.017-29.469,11.918-41.603,12.134h-0.217C571.61,1271.551,610.829,938.726,622.53,854.653z",
          thanksLeafColor = "#AED143";

      var leafSvgEl = angular.element("#leafs svg")[0],
          leafSnap = Snap( leafSvgEl ),
          leafPath = leafSnap.select( 'path' ),
          leafCurrentPath = leafPath.attr('d');


      if(stateName){
        checkState(stateName);
      }

      function checkState(stateName){
        switch(stateName){
            case "welcome"    :     changeLeafPath(welcomeLeafColor,welcomeLeafPath);
                                    makeMeInVisible();
                                    break;

            case "signup"     :     changeLeafPath(surveyLeafColor,surveyLeafPath);
                                    paddition = 0;
                                    bindSignaturePad();
                                    makeMeVisible();
                                    break;

            case "lifestyle"  :     changeLeafPath(surveyLeafColor,surveyLeafPath);
                                    paddition = 5;
                                    makeMeVisible();
                                    break;

            case "meter"      :     changeLeafPath(surveyLeafColor,surveyLeafPath);
                                    paddition = 10;
                                    makeMeVisible();
                                    moreVideos();
                                    break;

            case "scorecard"  :     changeLeafPath(scorecardLeafColor,scorecardLeafPath);
                                    moreVideos();
                                    makeMeInVisible();
                                    break;

            case "thanks"     :     changeLeafPath(thanksLeafColor,thanksLeafPath);
                                    moreVideos();
                                    makeMeInVisible();
                                    break;
        }
      }

      function  makeMeVisible() {
        $timeout( function(){
           angular.element(".navigation").addClass("active");
        }, 1000 );
      };

      function  makeMeInVisible() {
          angular.element(".navigation").removeClass("active");
      };

      function changeLeafPath(fill,d) {
          leafPath.animate( {  'path' : d , 'fill' : fill }, 500, mina.easeout);
      }

      function moreVideos() {
        $(document).ready(function () {
          var btn = document.querySelector(".wmv");
          if(btn){
            var srcCOntentId = "#hidden-content-"+$rootScope.videos_list_id;
            btn.addEventListener("click",function () {
                $.fancybox.open({
                  src  : srcCOntentId,
                  type : 'inline',
                  opts : {
                    afterShow : function( instance, current ) {
                    }
                  }
                });
            });
          }
        });
      }

      function signPadReset() {
        $(document).ready(function () {
          var canvas = angular.element(".signature").find("canvas")[0];
          var _signaturePad = new SignaturePad(canvas);
          _signaturePad.clear($rootScope.needToBeSigned=true);
        });
      }

    };
      

    animateLeaf();

    $scope.getBgClass = function () {
      if(bodyClass) return bodyClass;
    };

  });

  $scope.prevScreen = function () {
      if(stateName == "meter"){
        navigateTo = "lifestyle"
      } else if(stateName == "lifestyle"){
        navigateTo = "signup"
      }
      $state.go(navigateTo);
  };

  $rootScope.pageNum = 1;

  $rootScope.updateUserInfo = function (name,age,ms,noc,sign) {

  };
  $rootScope.updateHolidayMeter = function (A,B,C,D,E) {
      var _total_points = calculateScore(A) + calculateScore(B) + calculateScore(C) + calculateScore(D) + calculateScore(E);
      var _percentage = (_total_points/100)*100;
      holidayMeterScore = Math.round(_percentage);
  };

  $rootScope.getMeterPercentage = function (A,B,C,D,E) {
      var _total_points = calculateScore(A) + calculateScore(B) + calculateScore(C) + calculateScore(D) + calculateScore(E);
      var _percentage = (_total_points/100)*100;
      return Math.round(_percentage);
  };

  $rootScope.updateLifeStyle = function (A,B,C,D,E) {

  };

  $rootScope.getParagraph = function (A,B,C,D) {
      var A = parseInt(A);
      var B = parseInt(B);
      var C = parseInt(C);
      //console.log(A+" "+B+" "+C);
      var _result_para = "";
      if( ( A == 2 && B == 0  ) || ( A == 2 && B == 1  ) || ( A == 2 && B == 2  ) || ( A == 2 && B == 3  ) ){
          _result_para = "You are hard working! But you are missing out on some of the amazing experiences that happen out of your comfort zone. You definitely need a break!";
      }
      if( ( A == 1 && B == 0  ) || ( A == 1 && B == 1  ) || ( A == 1 && B == 2  ) || ( A == 1 && B == 3  ) ){
        _result_para = "Glad to know – You are not stressed ☺ Go for more vacations to maintain the balance.";
      }
      if( ( A == 0 && B == 0  ) || ( A == 0 && B == 1  ) || ( A == 0 && B == 2  ) || ( A == 0 && B == 3  ) ){
          _result_para = "You are blessed and enjoying life! We recommend you to experience our unique vacation packages.";
      }

      lifeStyleParagraph = _result_para;
      $rootScope.lifeStyleParagraph = lifeStyleParagraph;
      $rootScope.videos_list_id = D;
  };

  $rootScope.checkHmValidation = function () {
      $rootScope.hShhowMsgs = false;
  };

  $rootScope.checkLifeValidation = function () {
      $rootScope.lShhowMsgs = false;
       
  };

   function bindSignaturePad() {
    $(document).ready(function () {
      var canvas = angular.element(".signature").find("canvas")[0];
      var _signaturePad = new SignaturePad(canvas, {
          onEnd : function () {
            $rootScope.needToBeSigned = false;
            signData = _signaturePad.toDataURL();
          }
      });
    });
  };

  $rootScope.userInfoDataArray = {};
  $rootScope.lifestyleData = {};
  $rootScope.metereData = {};

  calculateScore = function (choice) {
    var ch = parseInt(choice);
    var _value = 0;
      switch(ch){
        case 0 : _value = 20;
                 break;
        case 1 : _value = 15;
                 break;
        case 2 : _value = 12;
                 break;
        case 3 : _value = 7;
                 break;
        case 4 : _value = 3;
                 break;
      }
      return _value;
  };



}]);

cmSurvey.controller("signupCtrl", ["$rootScope","$scope",function($rootScope,$scope) {
  $rootScope.needToBeSigned = true;
  $scope.signUpInfo = function (e) {
      $rootScope.userInfoDataArray.sign = signData;
  };
  $scope.toggleSign = function () {
    $rootScope.needToBeSigned = true;
  }
}]);

cmSurvey.controller("meterCtrl", ["$rootScope","$scope",function($rootScope,$scope) {
  $rootScope.needToBeSigned = true;
  $scope.signUpInfo = function (e) {
  };
  $scope.toggleSign = function () {
    $rootScope.needToBeSigned = true;
  }
}]);


cmSurvey.controller("loginCtrl", ["$scope",function($scope) {
}]);

cmSurvey.controller("thanksCtrl", ["$scope","$location","$window",function($scope,$location, $window) {
  $scope.reSurvey = function () {
      $window.location.reload();
  }
}]);

cmSurvey.controller("welcomeCtrl", ["$scope",function($scope) {
}]);

cmSurvey.config(['slickCarouselConfig', function (slickCarouselConfig) {
    slickCarouselConfig.dots = false;
    slickCarouselConfig.autoplay = false;
  }]).controller('questionearCtrl',["$rootScope","$scope","$timeout","$state", function ($rootScope,$scope, $timeout,$state) {
    $scope.navActive = true; 
    $scope.slickConfig1Loaded = true;
    $scope.slickCurrentIndex = 0;
    $rootScope.slickConfig = {
      dots: false,
      autoplay: false,
      initialSlide: 0,
      infinite: false,
      slidesToScroll: 1,
      variableWidth: true,
      arrows:false,
      method: {},
      event: {
        afterChange: function (event, slick, currentSlide, nextSlide) {
          $scope.slickCurrentIndex = currentSlide;
          $rootScope.pageNum = paddition+currentSlide+1;
          //console.log("\n Changed \npageNum : "+$rootScope.pageNum+" Paddition : "+paddition+ " Current Slide :"+currentSlide );
          prevScreenFn();
        },
        breakpoint: function (event, slick, breakpoint) {

        },
        init: function (event, slick) {
          $rootScope.pageNum = paddition+1;
          prevScreenFn();
          //console.log("\n Init \n pageNum : "+$rootScope.pageNum+" Paddition : "+paddition );
          document.addEventListener('keyup', function (e) {
            if(e.keyCode == 13 || e.keyCode == 9){
              $scope.slickConfig.method.slickNext();
            }
          });

          var lbls = document.querySelectorAll(".card .inputs label");
          for (var i = 0;i < lbls.length; i++) {
            lbls[i].addEventListener('click', function() {
              $scope.slickConfig.method.slickNext();
            });
          }
          var workOfWeek = $(":radio[name='workinweek']");
          var vaccationType = $(":radio[name='vacationinyear']");

          workOfWeek.on("click",function(){
            var _sval = $(this).val();
            if(_sval < 2){
              $(vaccationType[2]).attr("disabled","disabled");
              $(vaccationType[3]).attr("disabled","disabled");
              $(vaccationType).prop("checked",false);
              $scope.resetForm();
            } else{
              $(vaccationType[2]).removeAttr("disabled");
              $(vaccationType[3]).removeAttr("disabled");
            }
          });
          vaccationType.on("click",function(){
            $scope.validateFrom();
          });
        },
        setPosition: function (evnet, slick) {

        }
        
      }
    };

    function prevScreenFn() {
      if($rootScope.pageNum == 6 || $rootScope.pageNum == 11){
        $(".back.prev-back").show();
        $(".back.slick-back").hide();
      } else{
        $(".back.prev-back").hide();
        $(".back.slick-back").show();
      }
    }
     

    

    $scope.resetForm = function () {
      $scope.this.userLifestyleForm.vacationinyear.$setValidity('someValidator', null);
    };

    $scope.validateFrom = function () {
      $scope.this.userLifestyleForm.vacationinyear.$setValidity('someValidator', true);
    };
    

    $scope.makeMeActive = function (item) {
      var i = item.currentTarget.parentElement.parentElement.dataset.slickIndex;
      if(!(i==$scope.slickCurrentIndex))
        $scope.slickConfig.method.slickGoTo(i);
    };

}]);

cmSurvey.controller("lifestyleCtrl", ["$scope",function($scope) {
}]);

cmSurvey.controller("meterCtrl", ["$scope",function($scope) {
}]);


cmSurvey.controller("scorecardCtrl", ["$rootScope","$scope",function($rootScope,$scope) {
  $scope.$on('$viewContentLoaded', function(e){ 
      $rootScope.getParagraph($rootScope.lifestyleData.A,$rootScope.lifestyleData.B,$rootScope.lifestyleData.C,$rootScope.lifestyleData.D);
      var percentage = $rootScope.getMeterPercentage($rootScope.metereData.A,$rootScope.metereData.B,$rootScope.metereData.C,$rootScope.metereData.D,$rootScope.metereData.E);
      $scope.percentage = percentage+"%";
      var r = 100;
      var circles = document.querySelectorAll('.circle');
      var total_circles = circles.length;
      for (var i = 0; i < total_circles; i++) {
          circles[i].setAttribute('r', r);
      }
      var meter_dimension = (r * 2) + 200;
      var wrapper = document.querySelector('#wrapper');
      wrapper.style.width = meter_dimension + 'px';
      wrapper.style.height = meter_dimension + 'px';
      var cf = 2 * Math.PI * r;
      var semi_cf = cf / 2;
      var semi_cf_1by3 = semi_cf / 3;
      var semi_cf_2by3 = semi_cf_1by3 * 2;
      document.querySelector('#outline_curves').setAttribute('stroke-dasharray', semi_cf + ',' + cf);
      document.querySelector('#low').setAttribute('stroke-dasharray', semi_cf + ',' + cf);
      document.querySelector('#avg').setAttribute('stroke-dasharray', semi_cf_2by3 + ',' + cf);
      document.querySelector('#high').setAttribute('stroke-dasharray', semi_cf_1by3 + ',' + cf);
      document.querySelector('#outline_ends').setAttribute('stroke-dasharray', 2 + ',' + (semi_cf - 2));
      document.querySelector('#mask').setAttribute('stroke-dasharray', semi_cf + ',' + cf);
      var mask = document.querySelector('#mask');
      var meter_needle =  document.querySelector('#meter_needle');
      function range_change_event(p) {
          var percent = p;
          var meter_value = semi_cf - ((percent * semi_cf) / 100);
          mask.setAttribute('stroke-dasharray', meter_value + ',' + cf);
          meter_needle.style.transform = 'rotate(' + (270 + ((percent * 180) / 100)) + 'deg)';
      }
      setTimeout(range_change_event(percentage),5000);
  });

}]);

cmSurvey.run(['$rootScope', '$http', '$location', '$localStorage','$state', function($rootScope, $http, $location, $localStorage,$state) {
    if ($localStorage.currentUser) {
        $http.defaults.headers.common.Authorization = 'Bearer ' + $localStorage.currentUser.token;
        loginAuth = true;
    }
    $rootScope.$on('$locationChangeStart', function (event, next, current) {
        var publicPages = ['/login'];
        var restrictedPage = publicPages.indexOf($location.path()) === -1;
        if (restrictedPage && $localStorage.currentUser === undefined) {
            $location.path('login');
        }
    });
}]);

cmSurvey.controller('LoginController',['$scope', '$rootScope', '$location', 'AuthenticationService','$state',
  function ($scope,  $rootScope, $location, AuthenticationService,$state) {
      AuthenticationService.Logout();
      $scope.dataLoading = true;
      $scope.login = function () {
          $scope.dataLoading = true;
          AuthenticationService.Login($scope.username, $scope.password, function (result) {
              if (result === true) {
                  $state.go('welcome');
                  loginAuth = true;
              } else {
                  $scope.dataLoading = false;
              }
          });
      };
  }
]);

cmSurvey.factory('AuthenticationService',['$http', '$localStorage',function($http, $localStorage) {
        var service = {};
        service.Login = Login;
        service.Logout = Logout;
        return service;
        function Login(username, password, callback) {
                fetch('http://159.65.159.58:8080/api/api/login',
                {
                  method: 'POST',
                  headers: new Headers({
                    'Accept': 'application/json',
                    'content-type' : 'application/x-www-form-urlencoded' ,
                  }),
                  body:"username="+username+"&password="+password
                }).then((response) => {
                    if (response.status===200) {
                      return response.json();
                    }
                    return response;
                }).then((responseJson) => {
                    if (responseJson.access_token) {
                        $localStorage.currentUser = { username: username, token: responseJson.access_token };
                        $http.defaults.headers.common.Authorization = 'Bearer ' + responseJson.access_token;
                        callback(true);
                    } else {
                        callback(false);
                    }
                }).catch((error) => {
                  console.error(error);
                });
        }
        function Logout() {
            delete $localStorage.currentUser;
            $http.defaults.headers.common.Authorization = '';
        }
    }
]);
 

 
cmSurvey.controller("videoCtrl",['$scope','$state','$timeout',function($scope,$state,$timeout) {
  $scope.videopopup = function (id) {
        //console.log("event " + e);
        //var videoId = e.currentTarget.attributes[2].nodeValue;
        var videoId = id;
        var url = "https://www.youtube.com/watch?v="+videoId+"&autoplay=1&rel=0&controls=0&showinfo=0&disablekb=1";
        $.fancybox.open({
          src : url,
          opts : {
            iframe: {
                tpl:'<iframe id="fancybox-frame-45720" name="fancybox-frame-45720" class="fancybox-iframe" frameborder="0" vspace="0" hspace="0" webkitAllowFullScreen mozallowfullscreen allowFullScreen allowtransparency="true" src=""></iframe>',
            },
            beforeShow : function( instance, current ) {
              var id = "fancybox-frame-45720";
              $(document).ready(function () {
                  var player = new YT.Player(id, {
                        events: {
                            'onReady': onPlayerReady,
                            'onStateChange': onPlayerStateChange
                        }
                  });
              });
            }
          }
        });
  }

  function onPlayerReady(event) {
      event.target.playVideo();
  }

  function onPlayerStateChange(event) {
      if (event.data === 0) {
        $.fancybox.destroy();
        $state.go("thanks");
      }
  }
}]);
